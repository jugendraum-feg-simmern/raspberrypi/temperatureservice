/*!
 * \file temperature_controller_pi.cpp
 * \brief Source der TemperaturControllerPi Klasse
 */
#include "inc/temperature_controller_pi.h"

TemperatureControllerPi::TemperatureControllerPi(QObject *parent, QString device_name)
{
    qDebug() << Q_FUNC_INFO;

    // move parameters to private variables
    device_name_ = device_name;
    has_fan_ = false;

    // does all universal set up
    init();
}

double TemperatureControllerPi::readTemperature()
{
    int temp = 0;
    QFile pi_cpu_temp("/sys/class/thermal/thermal_zone0/temp");
    if (!pi_cpu_temp.open(QIODevice::ReadOnly))
        log(LOGFILE, "[ PI ] Error reading temp of pi from /sys/class/thermal/thermal_zone0/temp");
    else
        temp = pi_cpu_temp.readLine().chopped(1).toInt();
    pi_cpu_temp.close();

    return temp/1000.;
}

void TemperatureControllerPi::init()
{
    qDebug() << Q_FUNC_INFO;

    // setup a timer for updating
    update_timer_ = new QTimer(this);
    connect(update_timer_, SIGNAL(timeout()), this, SLOT(update()));
    update_timer_->setInterval(UPDATE_CYCLE_S * 1000);
}
